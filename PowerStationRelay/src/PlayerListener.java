import com.powerstationmc.core.PowerStationCore;
import com.powerstationmc.core.database.DatabaseConnector;
import com.powerstationmc.core.util.TaskUtility;
import lombok.RequiredArgsConstructor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.sql.SQLException;
import java.time.LocalDateTime;

@RequiredArgsConstructor
public class PlayerListener implements Listener {

    private final PowerStationRelay plugin;

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player ply = event.getPlayer();
        DatabaseConnector dbc = PowerStationCore.getInstance().getDatabaseConnector();

        TaskUtility.schedulePlayerDataTask(ply, plugin, () -> {
            try {
                dbc.executeUpdate(plugin, "game_playerdata", "AddCorePlayerDataEntry",
                        ply.getName(), ply.getUniqueId(), LocalDateTime.now());
                //TODO Add other data entries (or probably not)

                //TODO Safely transfer player to hub
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }
}
