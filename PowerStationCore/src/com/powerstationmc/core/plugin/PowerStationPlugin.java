package com.powerstationmc.core.plugin;

import lombok.Getter;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public abstract class PowerStationPlugin extends JavaPlugin {

    @Getter
    private final YamlConfiguration hikariConfig;
    private final Map<String, String> queryMap;

    public PowerStationPlugin() {
        queryMap = new HashMap<>();

        InputStream cfgStream = getResource("hikari.yml");
        if (cfgStream == null) {
            hikariConfig = new YamlConfiguration();
        } else {
            hikariConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(cfgStream));
            ConfigurationSection cfgSection = hikariConfig.getConfigurationSection("Queries");
            if (cfgSection != null) {
                for (String key : cfgSection.getKeys(false)) {
                    queryMap.put(key, cfgSection.getString(key));
                }
            }
        }
    }

    public String getQuery(String key) {
        return queryMap.get(key);
    }

}
