package com.powerstationmc.core.module;

import java.util.HashMap;
import java.util.Map;

public class ModuleRegistry {

    private final Map<Class<? extends Module>, Module> modules;

    public ModuleRegistry() {
        modules = new HashMap<>();
    }

    public void registerModule(ModuleType type, Module module) {
        Class<? extends Module> typeClass = type.getType();
        if (!typeClass.isAssignableFrom(module.getClass())) {
            throw new IllegalArgumentException("Module class doesn't match type");
        }
        if (modules.containsKey(typeClass)) {
            throw new IllegalStateException("Type is already registered");
        }

        modules.put(typeClass, module);
        //TODO log console message
    }

    public <T extends Module> T getModule(Class<T> type) {
        return (T) modules.get(type); //Safe cast given map is carefully modified
    }

    public Module getModule(ModuleType type) {
        return modules.get(type.getType());
    }

}
