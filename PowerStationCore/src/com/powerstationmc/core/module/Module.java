package com.powerstationmc.core.module;

public interface Module {

    /**
     * Get the name of this module to be displayed several places. TODO where?
     *
     * @return name
     */
    String getName();

}
