package com.powerstationmc.core.module;

import com.powerstationmc.core.module.lily.Lily;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ModuleType {

    LILY(Lily.class);

    @Getter
    private final Class<? extends Module> type;

}
