package com.powerstationmc.core.module.lily;

@FunctionalInterface
public interface ServerMessageListener {

    void onMessage(LilyServer server, String channel, String message);

}
