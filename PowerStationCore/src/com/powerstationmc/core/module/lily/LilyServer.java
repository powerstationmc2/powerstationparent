package com.powerstationmc.core.module.lily;

import com.powerstationmc.core.util.SimpleFuture;

public interface LilyServer {

    boolean isAlive();

    void sendMessage(String channel, String message);

    SimpleFuture<String> query(String id, String message);

    String getName();

}
