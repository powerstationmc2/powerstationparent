package com.powerstationmc.core.module.lily;

@FunctionalInterface
public interface ServerQueryHandler {

    String onQuery(String message);

}
