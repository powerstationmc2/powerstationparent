package com.powerstationmc.core.module.lily;

import com.powerstationmc.core.module.Module;

public interface Lily extends Module {

    LilyServer getServer(String name);

    void registerMessageListener(ServerMessageListener listener);

    void registerQueryHandler(String id, ServerQueryHandler handler);

    void broadcast(String channel, String message);

}
