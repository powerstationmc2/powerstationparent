package com.powerstationmc.core.command;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlayerCommand extends Command {

    public PlayerCommand(PlayerCommandHandlerDelegate handlerDelegate) {
        super(handlerDelegate);
    }

    public PlayerCommand(PlayerCommandHandler handler) {
        this(new PlayerCommandHandlerDelegate(handler));
    }

    @Override
    protected CommandResult onCommand(CommandSender sender, ArgumentList args) {
        if (args.getDepth() != 0 || sender instanceof Player) {
            return super.onCommand(sender, args);
        } else {
            return CommandResult.NOT_PLAYER;
        }
    }
}
