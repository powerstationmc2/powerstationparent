package com.powerstationmc.core.command;

import org.bukkit.command.CommandSender;

public interface CommandHandler {

    CommandResult handle(CommandSender sender, ArgumentList args);

}
