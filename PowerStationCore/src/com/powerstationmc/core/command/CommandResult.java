package com.powerstationmc.core.command;

public enum CommandResult {

    SUCCESS,
    INVALID_SYNTAX,
    MISSING_PERMS,
    NOT_PLAYER;

}
