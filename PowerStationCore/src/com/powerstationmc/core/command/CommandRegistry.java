package com.powerstationmc.core.command;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.SimplePluginManager;

import java.lang.reflect.Field;
import java.util.Arrays;

public class CommandRegistry {

    private final CommandMap cmdMap;

    public CommandRegistry() {
        try {
            PluginManager pm = Bukkit.getPluginManager();
            Field f = SimplePluginManager.class.getDeclaredField("commandMap");
            f.setAccessible(true);
            cmdMap = (CommandMap) f.get(pm);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException("Could not instantiate CommandRegistry", e);
        }
    }

    public void registerCommand(Command command, String name, String description, String usageMessage, String... aliases) {
        org.bukkit.command.Command cmd = new PowerStationCommandImpl(command, name, description, usageMessage, aliases);
        cmdMap.register("psmc", cmd);
    }

    private static final class PowerStationCommandImpl extends org.bukkit.command.Command {

        private final Command command;

        PowerStationCommandImpl(Command command, String name, String description, String usageMessage, String[] aliases) {
            super(name, description, usageMessage, Arrays.asList(aliases));
            this.command = command;
        }

        @Override
        public boolean execute(CommandSender sender, String label, String[] args) {
            command.onCommand(sender, new ArgumentList(args));
            return false;
        }

    }

}
