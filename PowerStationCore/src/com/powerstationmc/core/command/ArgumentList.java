package com.powerstationmc.core.command;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ArgumentList {

    private final String[] args;
    @Getter
    private int depth;

    public String get(int index) {
        return getShallow(index + depth);
    }

    public String getShallow(int index) {
        return args[index];
    }

    public int length() {
        return fullLength() - depth;
    }

    public int fullLength() {
        return args.length;
    }

    void addDepth() {
        depth++;
    }

}
