package com.powerstationmc.core.command;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.command.CommandSender;

import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
public class Command {

    @Getter
    private final CommandHandler handler;

    private final Map<String, Command> subCommands = new HashMap<>();

    public void registerSubCommand(Command command, String... aliases) {
        for (String alias : aliases) {
            subCommands.put(alias, command);
        }
    }

    protected CommandResult onCommand(CommandSender sender, ArgumentList args) {
        if (args.length() >= 1) {
            Command subCommand = subCommands.get(args.get(0));
            if (subCommand != null) {
                args.addDepth();
                return subCommand.onCommand(sender, args);
            }
        }

        return handler.handle(sender, args);
    }

}
