package com.powerstationmc.core.command;

import lombok.RequiredArgsConstructor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Doesn't instanceof check. Only casts.
 */
@RequiredArgsConstructor
public class PlayerCommandHandlerDelegate implements CommandHandler {

    private final PlayerCommandHandler handler;

    @Override
    public CommandResult handle(CommandSender sender, ArgumentList args) {
        return handler.handle((Player) sender, args);
    }

}
