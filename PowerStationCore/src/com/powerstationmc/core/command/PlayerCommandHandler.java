package com.powerstationmc.core.command;

import org.bukkit.entity.Player;

public interface PlayerCommandHandler {

    CommandResult handle(Player ply, ArgumentList args);

}
