package com.powerstationmc.core.player;

import com.powerstationmc.core.PowerStationCore;
import com.powerstationmc.core.database.DatabaseConnector;
import com.powerstationmc.core.util.TaskUtility;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PlayerList<T extends PowerStationPlayer> {

    private final PowerStationCore plugin;
    private final DatabaseConnector dbc;

    private final Class<T> clazz;
    private final Constructor<T> constructor;

    private final Map<String, List<PlayerDataField>> dataFields = new HashMap<>();
    private final Map<Player, T> players = new ConcurrentHashMap<>();

    public PlayerList(PowerStationCore plugin, Class<T> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) {
            throw new IllegalArgumentException("Class must not be abstract");
        }

        this.plugin = plugin;
        dbc = plugin.getDatabaseConnector();

        this.clazz = clazz;
        try {
            constructor = clazz.getDeclaredConstructor(Player.class);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("No (Player) constructor found in " + clazz.getName(), e);
        }

        generateFieldMap();
    }

    void checkinPlayer(Player ply) {
        try {
            T psPlayer = constructor.newInstance(ply);
            players.put(ply, psPlayer);

            //Lock on Player object because PSID is unavailable at this point
            TaskUtility.schedulePlayerDataTask(ply, plugin, () -> {
                try {
                    ResultSet rs = getCorePlayerData(ply);
                    if (rs.next()) {
                        int psid = rs.getInt("psid");
                        String name = rs.getString("name");
                        if (!ply.getName().equals(name)) {
                            dbc.executeUpdate(plugin, "game_playerdata", "UpdateCorePlayerName", name, psid);
                        }
                        psPlayer.setPowerPoints(rs.getInt("powerpoints"));
                        psPlayer.setFirstLogin(rs.getObject("firstlogin", LocalDateTime.class));
                        psPlayer.setSavedPlayTime(rs.getInt("playtime"));
                        psPlayer.setPowerStationId(psid);

                        //TODO Load other data
                        //TODO consider loading from cached data from player's previous server (if any)
                    } else {
                        throw new SQLException("Playerdata not found in database");
                    }
                } catch (SQLException e) {
                    plugin.getLogger().severe("Unable to instantiate player '" + ply.getName() + "'");
                    plugin.getLogger().severe("Kicking player");
                    ply.kickPlayer("Database error!");

                    e.printStackTrace();
                }
            });
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    void checkoutPlayer(Player ply) {
        T psPlayer = players.remove(ply);
        if (psPlayer != null) {
            //TODO save all data
            //TODO consider transferring to player's new server instead of saving (unless total quit)
        }
    }

    private ResultSet getCorePlayerData(Player ply) throws SQLException {
        return dbc.executeQuery(plugin, "game_playerdata", "GetCorePlayerData", ply.getUniqueId().toString());
    }

    private void generateFieldMap() {
        Class<?> c = clazz;
        do {
            String defaultTable;
            PlayerDataTable pdt = c.getDeclaredAnnotation(PlayerDataTable.class);
            if (pdt == null || pdt.value().isEmpty()) {
                defaultTable = null;
            } else {
                defaultTable = pdt.value();
            }
            for (Field field : c.getDeclaredFields()) {
                PlayerData pd = field.getAnnotation(PlayerData.class);
                if (pd == null) {
                    continue;
                }

                String table = pd.table();
                String column = pd.column();
                if (table.isEmpty()) {
                    if (defaultTable == null) {
                        //TODO log severe or back out entirely
                        //TODO probably latter
                        continue;
                    } else {
                        table = defaultTable;
                    }
                }
                if (column.isEmpty()) {
                    column = field.getName();
                }

                List<PlayerDataField> dataFieldList = dataFields.get(table);
                if (dataFieldList == null) {
                    dataFieldList = new ArrayList<>();
                    dataFields.put(table, dataFieldList);
                }
                dataFieldList.add(new PlayerDataField(column, field));
            }
        } while ((c = c.getSuperclass()) != PowerStationPlayer.class);
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    final class PlayerDataField {

        final String column;
        final Field field;

    }

}
