package com.powerstationmc.core.player;

import lombok.RequiredArgsConstructor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

@RequiredArgsConstructor
public class PlayerListener implements Listener {

    private final PlayerList<?> playerList;

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        playerList.checkinPlayer(event.getPlayer());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        playerList.checkoutPlayer(event.getPlayer());
    }

}
