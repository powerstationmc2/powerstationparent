package com.powerstationmc.core.player;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

import java.time.LocalDateTime;
import java.util.UUID;

public abstract class PowerStationPlayer {

    private volatile boolean loaded;

    @Getter
    private final Player handle;
    @Getter
    private final String name;
    @Getter
    private final UUID uuid;
    private int psid; //Inject new value

    @Getter
    private final LocalDateTime sessionStart;
    @Setter(AccessLevel.PACKAGE)
    @Getter
    private LocalDateTime firstLogin;
    private long lastPlayTimeSave;
    @Setter(AccessLevel.PACKAGE)
    private int savedPlayTime;

    @Getter
    @Setter
    private int powerPoints;

    public PowerStationPlayer(Player handle) {
        this.handle = handle;
        this.name = handle.getName();
        this.uuid = handle.getUniqueId();

        this.sessionStart = LocalDateTime.now();
    }

    public final int getPowerStationId() {
        return psid;
    }

    final void setPowerStationId(int psid) {
        this.psid = psid;
    }

    public boolean alterPowerPoints(int delta, boolean force) {
        int newValue = powerPoints + delta;
        if (!force && delta < 0 && newValue < 0) {
            return false;
        }

        powerPoints = newValue;
        return true;
    }

    public boolean alterPowerPoints(int delta) {
        return alterPowerPoints(delta, false);
    }

    public final boolean hasLoaded() {
        return loaded;
    }

    final void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    /**
     * Combination of savedPlayTime and time since lastPlayTimeSave
     *
     * @return
     */
    public int getTotalPlayTime() {
        return (int) (savedPlayTime + System.currentTimeMillis() - lastPlayTimeSave);
    }

}
