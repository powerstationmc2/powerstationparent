package com.powerstationmc.core;

import com.powerstationmc.core.command.CommandRegistry;
import com.powerstationmc.core.database.DatabaseConnector;
import com.powerstationmc.core.module.ModuleRegistry;
import com.powerstationmc.core.player.PlayerList;
import com.powerstationmc.core.player.PlayerListener;
import com.powerstationmc.core.player.PowerStationPlayer;
import com.powerstationmc.core.plugin.PowerStationPlugin;
import lombok.Getter;

public class PowerStationCore extends PowerStationPlugin {

    //Used by other plugins
    @Getter
    private static PowerStationCore instance;

    @Getter
    private ModuleRegistry moduleRegistry;
    @Getter
    private CommandRegistry commandRegistry;
    @Getter
    private PlayerList<?> playerList;
    @Getter
    private DatabaseConnector databaseConnector;

    @Override
    public void onEnable() {
        instance = this;

        saveDefaultConfig();

        moduleRegistry = new ModuleRegistry();
        commandRegistry = new CommandRegistry();
        databaseConnector = new DatabaseConnector(this);

        databaseConnector.usingDatabases(this);
    }

    @Override
    public void onDisable() {
        instance = null;
    }

    public <T extends PowerStationPlayer> PlayerList<T> initialisePlayerList(Class<T> clazz) {
        if (!getConfig().getBoolean("Use-PlayerList")) {
            throw new IllegalStateException("Server configured not to use PlayerList");
        } else if (playerList != null) {
            throw new IllegalStateException("PlayerList already registered");
        }

        PlayerList<T> list = new PlayerList<>(this, clazz);
        playerList = list;
        registerPlayerListener();

        return list;
    }

    private void registerPlayerListener() {
        PlayerListener listener = new PlayerListener(playerList);
        getServer().getPluginManager().registerEvents(listener, this);
    }

}
