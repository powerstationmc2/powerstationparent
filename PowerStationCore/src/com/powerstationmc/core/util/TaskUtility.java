package com.powerstationmc.core.util;

import com.powerstationmc.core.player.PowerStationPlayer;
import com.powerstationmc.core.plugin.PowerStationPlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public final class TaskUtility {

    private TaskUtility() {
        //No instantiation
    }

    /**
     * Uses PSID. Preferred.
     *
     * @param ply
     * @param plugin
     * @param task
     * @return
     */
    public static BukkitTask schedulePlayerDataTask(PowerStationPlayer ply, PowerStationPlugin plugin, Runnable task) {
        return schedulePlayerDataTask(ply, plugin, task, 0L);
    }

    /**
     * Uses PSID. Preferred.
     *
     * @param ply
     * @param plugin
     * @param task
     * @param delay
     * @return
     */
    public static BukkitTask schedulePlayerDataTask(PowerStationPlayer ply, PowerStationPlugin plugin, Runnable task, long delay) {
        return Bukkit.getScheduler().runTaskLaterAsynchronously(plugin,  () -> {
            try {
                //TODO acquire database-lock on player
                task.run();
            } finally {
                //TODO release database-lock
            }
        }, delay);
    }

    /**
     * Uses UUID.
     *
     * @param ply
     * @param plugin
     * @param task
     * @return
     */
    public static BukkitTask schedulePlayerDataTask(Player ply, PowerStationPlugin plugin, Runnable task) {
        return schedulePlayerDataTask(ply, plugin, task, 0L);
    }

    /**
     * Uese UUID
     *
     * @param ply
     * @param plugin
     * @param task
     * @param delay
     * @return
     */
    public static BukkitTask schedulePlayerDataTask(Player ply, PowerStationPlugin plugin, Runnable task, long delay) {
        return Bukkit.getScheduler().runTaskLaterAsynchronously(plugin,  () -> {
            try {
                //TODO acquire database-lock on player
                task.run();
            } finally {
                //TODO release database-lock
            }
        }, delay);
    }

}
