package com.powerstationmc.core.util;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

public final class SimpleFuture<T> {

    private boolean done;
    private T result;

    private SimpleFuture() {

    }

    private synchronized void setResult(T result) {
        if (done) {
            throw new IllegalStateException("Result already set");
        }

        this.result = result;
        done = true;
        notifyAll();
    }

    public synchronized T await() throws InterruptedException {
        while (!done) {
            wait();
        }

        return result;
    }

    public static <T> ResultDelegate<T> getNew() {
        return new ResultDelegate<>(new SimpleFuture<>());
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    public static final class ResultDelegate<T> {

        @Getter
        private final SimpleFuture<T> future;

        public void setResult(T result) {
            future.setResult(result);
        }

    }

}
