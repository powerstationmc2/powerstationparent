package com.powerstationmc.core.database;

public class DatabaseNotRegisteredException extends RuntimeException {

    public DatabaseNotRegisteredException() {
        super();
    }

    public DatabaseNotRegisteredException(String message) {
        super(message);
    }

}
