package com.powerstationmc.core.database;

import com.powerstationmc.core.PowerStationCore;
import com.powerstationmc.core.plugin.PowerStationPlugin;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public class DatabaseConnector {

    @Getter
    private volatile boolean locked;

    private final Map<String, HikariDataSource> dataSources = new ConcurrentHashMap<>();
    private final Properties props;

    public DatabaseConnector(PowerStationCore plugin) {
        plugin.saveResource("hikari.properties", false);
        props = new Properties();
        try {
            File propsFile = new File(plugin.getDataFolder(), "hikari.properties");
            props.load(new FileInputStream(propsFile));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized void usingDatabases(PowerStationPlugin plugin) {
        if (isLocked()) {
            throw new IllegalStateException("DatabaseConnector is locked");
        }

        YamlConfiguration config = plugin.getHikariConfig();
        config.getStringList("Databases").forEach(this::usingDatabase);
    }

    /**
     * Call on enable
     *
     * @param database
     * @return
     */
    public synchronized void usingDatabase(String database) {
        if (isLocked()) {
            throw new IllegalStateException("DatabaseConnector is locked");
        }

        if (!dataSources.containsKey(database)) {
            Properties props = new Properties(this.props);
            props.setProperty("dataSource.databaseName", database);
            HikariConfig config = new HikariConfig(props);
            HikariDataSource ds = new HikariDataSource(config);

            dataSources.put(database, ds);
        }
    }

    /**
     * Blocks if all connections are busy
     *
     * @param database
     * @return
     */
    public Connection getConnection(String database) throws SQLException {
        HikariDataSource ds = dataSources.get(database);
        if (ds == null) {
            throw new DatabaseNotRegisteredException();
        }

        return ds.getConnection();
    }

    public int executeUpdate(String database, String sql, Object... parameters) throws SQLException {
        try (Connection con = getConnection(database); PreparedStatement stmt = con.prepareStatement(sql)) {
            for (int i = 0; i < parameters.length; i++) {
                stmt.setObject(i + 1, parameters[i]);
            }

            return stmt.executeUpdate();
        }
    }

    public int executeUpdate(PowerStationPlugin plugin, String database, String queryKey,
                             Object... parameters) throws SQLException {
        try (
                Connection con = getConnection(database);
                PreparedStatement stmt = con.prepareStatement(plugin.getQuery(queryKey))
        ) {
            for (int i = 0; i < parameters.length; i++) {
                stmt.setObject(i + 1, parameters[i]);
            }

            return stmt.executeUpdate();
        }
    }

    public ResultSet executeQuery(String database, String sql, Object... parameters) throws SQLException {
        try (Connection con = getConnection(database); PreparedStatement stmt = con.prepareStatement(sql)) {
            for (int i = 0; i < parameters.length; i++) {
                stmt.setObject(i + 1, parameters[i]);
            }

            return stmt.executeQuery();
        }
    }

    public ResultSet executeQuery(PowerStationPlugin plugin, String database, String queryKey,
                                  Object... parameters) throws SQLException {
        try (
                Connection con = getConnection(database);
                PreparedStatement stmt = con.prepareStatement(plugin.getQuery(queryKey))
        ) {
            for (int i = 0; i < parameters.length; i++) {
                stmt.setObject(i + 1, parameters[i]);
            }

            return stmt.executeQuery();
        }
    }

    public synchronized void close() {
        lock();
        for (HikariDataSource ds : dataSources.values()) {
            ds.close();
        }
    }

    public synchronized boolean lock() {
        if (isLocked()) {
            return false;
        } else {
            locked = true;
            return true;
        }
    }

}
