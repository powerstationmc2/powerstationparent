package com.powerstationmc.lily;

import com.powerstationmc.core.module.lily.LilyServer;
import com.powerstationmc.core.util.SimpleFuture;
import lilypad.client.connect.api.request.RequestException;
import lilypad.client.connect.api.request.impl.MessageRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Should be completely thread safe.
 */
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class PowerStationLilyServer implements LilyServer {

    private final PowerStationLily plugin;
    @Getter
    private final String name;

    private volatile boolean alive = true;

    private final Map<String, SimpleFuture.ResultDelegate<String>> queryResultDelegates = new ConcurrentHashMap<>();

    synchronized void kill() {
        alive = false;
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    private void checkAlive() {
        if (!alive) {
            throw new IllegalStateException("Server is dead");
        }
    }

    @Override
    public void sendMessage(String channel, String message) {
        checkAlive();
        sendMessage0(channel, message);
    }

    private synchronized void sendMessage0(String channel, String message) {
        checkAlive();
        try {
            channel = PowerStationLily.messageChannel(channel);
            plugin.getConnect().request(new MessageRequest(name, channel, message));
        } catch (RequestException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public SimpleFuture<String> query(String id, String message) {
        checkAlive();
        return query0(id, message);
    }

    private synchronized SimpleFuture<String> query0(String id, String message) {
        checkAlive();
        String uniqueQueryId = PowerStationLily.uniqueQueryId(id);
        String channel = PowerStationLily.queryChannel(uniqueQueryId);
        SimpleFuture.ResultDelegate<String> delegate = SimpleFuture.getNew();
        queryResultDelegates.put(uniqueQueryId, delegate);
        try {
            plugin.getConnect().request(new MessageRequest(name, channel, message));
        } catch (RequestException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return delegate.getFuture();
    }

    void handleQueryResult(String key, String message) {
        SimpleFuture.ResultDelegate<String> resultDelegate = queryResultDelegates.remove(key);
        if (resultDelegate == null) {
            //TODO log error
        } else {
            resultDelegate.setResult(message);
        }
    }

}
