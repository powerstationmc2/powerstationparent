package com.powerstationmc.lily;

import com.powerstationmc.core.PowerStationCore;
import com.powerstationmc.core.module.ModuleType;
import com.powerstationmc.core.module.lily.Lily;
import com.powerstationmc.core.module.lily.LilyServer;
import com.powerstationmc.core.module.lily.ServerMessageListener;
import com.powerstationmc.core.module.lily.ServerQueryHandler;
import com.powerstationmc.core.plugin.PowerStationPlugin;
import lilypad.client.connect.api.Connect;
import lilypad.client.connect.api.event.EventListener;
import lilypad.client.connect.api.event.MessageEvent;
import lombok.Getter;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//TODO Implement keepalives
public class PowerStationLily extends PowerStationPlugin implements Lily {

    /**
     * Synchronized access only!
     * PowerStationLily.class as monitor.
     */
    private static int queryNumber;

    public static final char CHANNEL_DELIMITER = '-';
    public static final String CHANNEL_PREFIX = "pslily";
    public static final String QUERY_CHANNEL_PREFIX = CHANNEL_PREFIX + CHANNEL_DELIMITER + "query";
    public static final String QUERY_RESULT_CHANNEL_PREFIX = QUERY_CHANNEL_PREFIX + "result";

    private final Map<String, PowerStationLilyServer> servers = new ConcurrentHashMap<>();
    private final List<ServerMessageListener> messageListeners = new ArrayList<>();

    private final Map<String, ServerQueryHandler> queryHandlers = new HashMap<>();

    @Getter
    private Connect connect;

    @Override
    public void onEnable() {
        PowerStationCore.getInstance().getModuleRegistry().registerModule(ModuleType.LILY, this);
        connect = Bukkit.getServicesManager().getRegistration(Connect.class).getProvider();
        connect.registerEvents(this);
    }

    @Override
    public void onDisable() {
        connect.unregisterEvents(this);
    }

    @Override
    public LilyServer getServer(String name) {
        return servers.get(name);
    }

    @Override
    public void registerMessageListener(ServerMessageListener listener) {
        messageListeners.add(listener);
    }

    @Override
    public void registerQueryHandler(String id, ServerQueryHandler handler) {
        if (queryHandlers.containsKey(id)) {
            throw new IllegalArgumentException(String.format("Handler already registered for id: '%s'", id));
        } else {
            queryHandlers.put(id, handler);
        }
    }

    @Override
    public void broadcast(String channel, String message) {
        servers.values().forEach(server -> server.sendMessage(channel, message));
    }

    @EventListener
    public void onMessage(MessageEvent event) throws UnsupportedEncodingException {
        PowerStationLilyServer sender = servers.get(event.getSender());
        if (sender == null) {
            //TODO Log error
            return;
        }
        String channel = event.getChannel();
        String message = event.getMessageAsString();
        String[] channelSplit = channel.split(String.valueOf(CHANNEL_DELIMITER));
        if (channel.matches(QUERY_CHANNEL_PREFIX + "-.+")) {
            if (channelSplit.length < 3) {
                //TODO log error
                return;
            }
            ServerQueryHandler handler = queryHandlers.get(channelSplit[1]);
            String resultChannel = channel.replaceAll("^" + QUERY_CHANNEL_PREFIX, QUERY_RESULT_CHANNEL_PREFIX);
            if (handler == null) {
                //TODO return error to sender
            } else {
                sender.sendMessage(resultChannel, handler.onQuery(message));
            }
        } else if (channel.matches(QUERY_RESULT_CHANNEL_PREFIX + "-.+")) {
            String key = StringUtils.join(channelSplit, CHANNEL_DELIMITER, 1, channelSplit.length);
            sender.handleQueryResult(key, message);
        } else if (channel.matches(CHANNEL_PREFIX + "-.+")) {
            String subChannel = StringUtils.join(channelSplit, CHANNEL_DELIMITER, 1, channelSplit.length);
            messageListeners.forEach(listener -> listener.onMessage(sender, subChannel, message));
        } else {
            //TODO Log that message is unhandled by this handler (or not)
        }
    }

    public static String messageChannel(String channel) {
        return CHANNEL_PREFIX + CHANNEL_DELIMITER + channel;
    }

    public static String queryChannel(String uniqueQueryId) {
        return QUERY_CHANNEL_PREFIX + CHANNEL_DELIMITER + uniqueQueryId;
    }

    public static synchronized String uniqueQueryId(String id) {
        return id + CHANNEL_DELIMITER + queryNumber++;
    }

}
